#include "project.hpp"

#include <chrono>
#include <cstdio>
#include <vector>
#include <algorithm>

/* The following defines the different visualization modes that you will
 * implement (several modes towards the end are optional, though).
 */
enum class EVisualizeMode
{
	// "none" is the default mode that you see when you first start the
	// application. It just draws the bounding box around the volume.
	none,

	// The following are the visualization modes that you will implement in the
	// different tasks.
	solidPoints,               // Task 1.
	additivePoints,            // Task 2
	colorAlphaPoints,          // Task 3 (3a + 3b)
	phongPoints,               // Task 4
	selectedPointsOnly,        // Task 5
	enhanceSelectedPoints,     // Optional Task 1
	billboards,                // Optional Task 2a
	billboardsWithLOD,         // Optional Task 2b
	drawAsArray,               // Optional Task 3a
	drawAsArrayFromVRAM        // Optional Task 3b, 3c, and 3d
};

// gVisualizeMode defines the currently selected visualization mode. See
// project_on_key_press(), which defines a few default keys to switch between
// the different modes. By default, you can do so with the 0-9 keys.
EVisualizeMode gVisualizeMode = EVisualizeMode::none;

// gVolume contains the currently loaded volume. See volume.hpp for an overview
// on how to access the data in the Volume class.
Volume gVolume( 0, 0, 0 );

// gVolumeLargestDimension is set to the largest dimensions of the volume. The
// volumes are not cubic...
std::size_t gVolumeLargestDimension = 0;

// gLightPosition contains the light position. This will be used for
// EVisualizeMode::phongPoints and latter visualization modes. You can change
// the light position to the current camera position by pressing 'l'. Return it
// to the default position with 'k'. See project_on_key_press() for details.
Vec3Df gLightPosition(2.f, 2.f, 0.f);

// gLightChanged is set to true when the light has changed. Use this to
// determine whether or not you need to update the data when the visualization
// mode is EVisualizeMode::drawAsArray or EVisualizeMode::drawAsArrayFromVRAM.
bool gLightChanged = false;

// The following defines the three types of regions by which you are asked to
// restrict the visible parts of the volume. The currently selected region 
// type is specified by 'gSelectiveRegionType' below.
enum class ESelectiveRegionType
{
	sphere,
	cube,
	slab
};

// gSelectiveRegionType is the currently selected region type by which to
// restrict which parts of the volume should be drawn in Task 5. You can
// switch the volume type using the keys 't' (sphere), 'g' (cube) and 'b'
// (slab).  See project_on_key_press() for details.
ESelectiveRegionType gSelectiveRegionType = ESelectiveRegionType::sphere;

// TODO: if you need store other information between frames or between the
// different functions, you may add it here:

// TODO: if you want to declare and define additional functions, you may add
// them here.

// project_initialize() is called once when the application is started. You may
// perform any one-time initialization here. By default, project_initialize()
// just loads the volume data.
bool project_initialize()
{
	// Try to load a volume
	// Switch here to load a different volume.
	gVolume = load_mhd_volume( "data/bonsai_small.mhd" );
	//gVolume = load_mhd_volume( "data/backpack_small.mhd" );

	if( 0 == gVolume.total_element_count() )
	{
		std::fprintf( stderr, "Error: couldn't load volume\n" );
		return false;
	}

	gVolumeLargestDimension = std::max( gVolume.width(), std::max( gVolume.height(), gVolume.depth() ) );
	
	// TODO: if you have other initialization code that needs to run once when
	// the program starts, you can place it here. (For example, Optional Tasks
	// 3{a,b,c,d} require such one-time initialization.)


	// Check for OpenGL errors.
	auto err = glGetError();
	if( GL_NO_ERROR != err )
	{
		std::fprintf( stderr, "OpenGL Error: %s\n", gluErrorString(err) );
		return false;
	}

	return true;
}

// project_draw_window() is called whenever the window needs to be redrawn.
// Just before project_draw_window(), the screen is cleared and the camera
// matrices are set up.
//
// project_draw_window() is where you will implement the various visualization
// modes.
void project_draw_window( Vec3Df const& aCameraFwd, Vec3Df const& aCameraUp )
{

	
	// We start by resetting the state that will eventually be modified.  If
	// you want to manage the OpenGL state by yourself more explicitly, you may
	// do so, but we suggest that you leave this as-is, and simply set up the
	// necessary state each time you use it.
	glPointSize( 1.f );
	glColor3f( 1.f, 1.f, 1.f );
	glDisable( GL_BLEND );

	// Make sure the volume is centered.
	Vec3Df volumeTranslation(
		float(gVolumeLargestDimension - gVolume.width()),
		float(gVolumeLargestDimension - gVolume.height()),
		float(gVolumeLargestDimension - gVolume.depth())
	);

	volumeTranslation /= float(gVolumeLargestDimension);
	glTranslatef( volumeTranslation[0], volumeTranslation[1], volumeTranslation[2] );


	// Select based on the current visualization mode. See EVisualizeMode and
	// gVisualizeMode above for more information.
	switch( gVisualizeMode )
	{
		case EVisualizeMode::none: {
			// Draw a placeholder for the volume.
			float const maxX = 2.f*float(gVolume.width())/gVolumeLargestDimension-1.f;
			float const maxY = 2.f*float(gVolume.height())/gVolumeLargestDimension-1.f;
			float const maxZ = 2.f*float(gVolume.depth())/gVolumeLargestDimension-1.f;

			glLineWidth( 2.f );
			glColor3f( 1.f, 1.0f, 0.2f );
			
			glBegin( GL_LINES );
				glVertex3f( -1.f, -1.f, -1.f ); glVertex3f( maxX, -1.f, -1.f );
				glVertex3f( -1.f, -1.f, -1.f ); glVertex3f( -1.f, maxY, -1.f );
				glVertex3f( -1.f, -1.f, -1.f ); glVertex3f( -1.f, -1.f, maxZ );

				glVertex3f( maxX, maxY, maxZ ); glVertex3f( -1.f, maxY, maxZ );
				glVertex3f( maxX, maxY, maxZ ); glVertex3f( maxX, -1.f, maxZ );
				glVertex3f( maxX, maxY, maxZ ); glVertex3f( maxX, maxY, -1.f );

				glVertex3f( maxX, -1.f, -1.f ); glVertex3f( maxX, maxY, -1.f );
				glVertex3f( -1.f, maxY, -1.f ); glVertex3f( -1.f, maxY, maxZ );
				glVertex3f( -1.f, -1.f, maxZ ); glVertex3f( maxX, -1.f, maxZ );

				glVertex3f( -1.f, maxY, maxZ ); glVertex3f( -1.f, -1.f, maxZ );
				glVertex3f( maxX, -1.f, maxZ ); glVertex3f( maxX, -1.f, -1.f );
				glVertex3f( maxX, maxY, -1.f ); glVertex3f( -1.f, maxY, -1.f );
			glEnd();
		} break;
		
		case EVisualizeMode::solidPoints: {
			// TODO your code for Task 1 goes here!
		} break;

		case EVisualizeMode::additivePoints: {
			//TODO: your code for Task 2 goes here!
		} break;

		case EVisualizeMode::colorAlphaPoints: {
			//TODO: your code for Task 3 goes here!
			// Note: start with Task 3a and then update it for Task 3b.
		} break;

		case EVisualizeMode::phongPoints: {
			//TODO: your code for Task 4 goes here!
		} break;

		case EVisualizeMode::selectedPointsOnly: {
			//TODO: your code for Task 5 goes here!
		} break;

		case EVisualizeMode::enhanceSelectedPoints: {
			//TODO: your code for Optional Task 1 goes here!
		} break;

		case EVisualizeMode::billboards: {
			//TODO: your code for Optional Task 2a goes here!
		} break;

		case EVisualizeMode::billboardsWithLOD: {
			//TODO: your code for Optional Task 2b goes here!
		} break;


		case EVisualizeMode::drawAsArray: {
			//TODO: your code for Optional Task 3a goes here!
		} break;

		case EVisualizeMode::drawAsArrayFromVRAM: {
			//TODO: your code for Optional Task 3b goes here!
			// Note: change this code for Optional Task 3d
		} break;
	}

	auto err = glGetError();
	if( GL_NO_ERROR != err )
		std::fprintf( stderr, "OpenGL Error: %s\n", gluErrorString(err) );
}



Vec3Df gradient( Volume const& aVolume, int aI, int aJ, int aK )
{
	//TODO: use this function to compute the gradient at the position (aI,aJ,aK)
	// Note: take special care at the borders! You must not read outside of the
	// defined volume. For example, if (aI-1) is less than zero or if (aI+1) is
	// larger than or equal to gVolume.width(), you will need to deal with that
	// manually!
	Vec3Df grad(0.f, 0.f, 0.f);


	return grad;
}

void project_on_key_press( int aKey, Vec3Df const& aCameraPos )
{
	switch( aKey )
	{
		// Keys 0-9 are used to switch between the different visualization
		// modes that you will implement.
		case '1': gVisualizeMode = EVisualizeMode::solidPoints; break;
		case '2': gVisualizeMode = EVisualizeMode::additivePoints; break;
		case '3': gVisualizeMode = EVisualizeMode::colorAlphaPoints; break;
		case '4': gVisualizeMode = EVisualizeMode::phongPoints; break;
		case '5': gVisualizeMode = EVisualizeMode::selectedPointsOnly; break;
		case '6': gVisualizeMode = EVisualizeMode::enhanceSelectedPoints; break;
		case '7': gVisualizeMode = EVisualizeMode::billboards; break;
		case '8': gVisualizeMode = EVisualizeMode::billboardsWithLOD; break;
		case '9': gVisualizeMode = EVisualizeMode::drawAsArray; break;
		case '0': gVisualizeMode = EVisualizeMode::drawAsArrayFromVRAM; break;

		// Keys 'l' and 'k' are used to change the light position. The light
		// position is used in Task 4 (Lighting).
		case 'l': gLightPosition = aCameraPos; gLightChanged = true; break;
		case 'k': gLightPosition = Vec3Df( 2.f, 2.f, 0.f ); gLightChanged = false; break;

		// Select the region type for Task 5 (Intersection).
		case 't': gSelectiveRegionType = ESelectiveRegionType::sphere; break;
		case 'g': gSelectiveRegionType = ESelectiveRegionType::cube; break;
		case 'b': gSelectiveRegionType = ESelectiveRegionType::slab; break;

		// Any remaining keys, you may use as you wish.
		// Suggestion: for Task 5 (Intersection), use e.g., 'w', 's', 'a', 'd',
		// 'e', 'q' to move the sphere/cube along the three axes, 'x' 'z' to
		// change the radius/size. Pick a good set of keys for the slab by
		// yourself. You may also use the project_interact_mouse_wheel() below.
	}
}

void project_interact_mouse_wheel( bool aUp )
{
	// You can use this function however you wish. Make sure it receives the
	// mouse wheel events.
}
